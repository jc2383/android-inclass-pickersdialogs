package com.example.pickersanddialogs;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

public class MainActivity extends AppCompatActivity {

    final String TAG = "JENELLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void chooseDatePressed(View view) {
        Log.d(TAG, "Date button pressed!");
    }
    public void chooseTimePressed(View view) {
        // write code to show the picker
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "timePicker");

    }
    public void showPopupPressed(View view) {
        Log.d(TAG, "Popup button pressed!");
    }


    // make a function
    public void abc(int hour, int minute) {
        String message = "User picked " + hour + ":" + minute;

        TextView resultsLabel = (TextView) findViewById(R.id.timeTextView);
        resultsLabel.setText(message);
    }


}
