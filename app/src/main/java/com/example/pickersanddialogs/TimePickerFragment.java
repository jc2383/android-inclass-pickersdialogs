package com.example.pickersanddialogs;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener
{
    final String TAG = "JENELLE";

    // 2. Delete all those default constructor and onCreateView functions

    // 3. Add a new function
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new TimePickerDialog(getActivity(), this, 7, 3, false);
    }


    // 1. Add this mandatory function
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // what do you want to do when the person presses OK on the time picker?
        // write the code here
        Log.d(TAG, "Person pressed ok on time picker");
        Log.d(TAG, "Selected time: " + hourOfDay + ":" + minute);

        // send the selected time back to MainActivity.java
        // -----------------
        // what screen are you going to send data to?
        MainActivity mainAct = (MainActivity) getActivity();

        // what do you want to do when you go back to MainActivity.java?
        mainAct.abc(hourOfDay,minute);
    }
}
